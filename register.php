<?php
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$username = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Validate username
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter a username.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM users WHERE username = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = trim($_POST["username"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "This username is already taken.";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter a password.";     
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password must have at least 6 characters.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm password.";     
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
        }
    }
    
    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
        
        // Prepare an insert statement
        $sql = "INSERT INTO users (username, password) VALUES (?, ?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_username, $param_password);
            
            // Set parameters
            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                header("location: login");
            } else{
                echo "Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    // Close connection
    mysqli_close($link);
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title>Register for AK CONSULT</title>
        <link rel="stylesheet" href="static/main.css"/>
    </head>
    <body>
        <nav class="container navbar navbar-light navbar-expand-sm">
            <div class="login">
                <span class="navbar-text">
                    <a href="login">Log in</a>
                    &mdash;
                    <a href="register">Register</a>
                </span>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Register for AK CONSULT <small>or <a href="login">log in</a></small></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>
                        AK CONSULT, JSC – steel workshop. The company established on 2007.
                        We design and manufacture standard and custom stainless steel products for
                        supermarkets, kitchens, medicine and industry.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <form method="POST">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" name="username" id="username" value="<?php echo $username; ?>" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" required autocomplete="username" autofocus />
                            <div class="invalid-feedback"><?php echo $username_err; ?></div>
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" value="<?php echo $password; ?>" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>" required />
                            <div class="invalid-feedback"><?php echo $password_err; ?></div>
                        </div>

                        <div class="form-group">
                            <label for="confirm_password">Confirm password</label>
                            <input type="password" name="confirm_password" id="confirm_password" value="<?php echo $confirm_password; ?>" class="form-control <?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>" required />
                            <div class="invalid-feedback"><?php echo $confirm_password_err; ?></div>
                        </div>

                        <button class="btn btn-primary pull-right" type="submit">Register</button>
                        <p class="clearfix"></p>
                    </form>
                </div>
                <div class="col-md-8 offset-md-2">
                    <div class="alert alert-warning">
                        <strong>Privacy notice</strong>: AK CONSULT collects the minimum amount of your personal information which is necessary to faciliate the features of our services. We do not collect or process any of your personal
                        information for the purposes of marketing or analytics. For details, please review our <a href="privacy" rel="noopener" target="_blank">privacy policy</a>.
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
