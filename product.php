<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login");
    exit;
}

require_once "config.php";
?>

<?php
// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: login");
    exit;
}
 
// Include config file
require_once "config.php";



// Define variables and initialize with empty values
$id = $title = $description = "";
$x = $y = $z = $quantity = "";
$params_err = "";

if ($_SERVER["REQUEST_METHOD"] == "GET" && is_numeric($_GET['id'])) {
    $result = mysqli_query($link, 'SELECT * FROM products WHERE id = ' . $_GET['id']);
    if ($row = mysqli_fetch_array($result)) {
        echo '<div class="event"><h4><a href="product?id=' . $row['id'] . '">' . $row['title'] . "</a></h4></div>";
        $id = $row['id'];
        $title = $row['title'];
        $description = $row['description'];
    }
}
mysqli_close($link);
?>

<?php

if($_SERVER["REQUEST_METHOD"] == "POST"){
 
 // Validate username
 if(empty(trim($_POST["username"]))){
     $username_err = "Please enter a username.";
 } else{
     // Prepare a select statement
     $sql = "SELECT id FROM users WHERE username = ?";
     
     if($stmt = mysqli_prepare($link, $sql)){
         // Bind variables to the prepared statement as parameters
         mysqli_stmt_bind_param($stmt, "s", $param_username);
         
         // Set parameters
         $param_username = trim($_POST["username"]);
         
         // Attempt to execute the prepared statement
         if(mysqli_stmt_execute($stmt)){
             /* store result */
             mysqli_stmt_store_result($stmt);
             
             if(mysqli_stmt_num_rows($stmt) == 1){
                 $username_err = "This username is already taken.";
             } else{
                 $username = trim($_POST["username"]);
             }
         } else{
             echo "Oops! Something went wrong. Please try again later.";
         }

         // Close statement
         mysqli_stmt_close($stmt);
     }
 }
 
 // Validate password
 if(empty(trim($_POST["password"]))){
     $password_err = "Please enter a password.";     
 } elseif(strlen(trim($_POST["password"])) < 6){
     $password_err = "Password must have at least 6 characters.";
 } else{
     $password = trim($_POST["password"]);
 }
 
 // Validate confirm password
 if(empty(trim($_POST["confirm_password"]))){
     $confirm_password_err = "Please confirm password.";     
 } else{
     $confirm_password = trim($_POST["confirm_password"]);
     if(empty($password_err) && ($password != $confirm_password)){
         $confirm_password_err = "Password did not match.";
     }
 }
 
 // Check input errors before inserting in database
 if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
     
     // Prepare an insert statement
     $sql = "INSERT INTO users (username, password) VALUES (?, ?)";
      
     if($stmt = mysqli_prepare($link, $sql)){
         // Bind variables to the prepared statement as parameters
         mysqli_stmt_bind_param($stmt, "ss", $param_username, $param_password);
         
         // Set parameters
         $param_username = $username;
         $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
         
         // Attempt to execute the prepared statement
         if(mysqli_stmt_execute($stmt)){
             // Redirect to login page
             header("location: login");
         } else{
             echo "Something went wrong. Please try again later.";
         }

         // Close statement
         mysqli_stmt_close($stmt);
     }
 }
 
 // Close connection
 mysqli_close($link);
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title>Catalog - AK CONSULT</title>
        <link rel="stylesheet" href="static/main.css"/>
    </head>
    <body>
        <nav class="container navbar navbar-light navbar-expand-sm">
            <div class="login">
                <span class="navbar-text">
                    Logged in as <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b> — <a href="logout">Log out</a>
                </span>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <p>You can browse our catalog here.</p>
                    </section>
                </div>
            </div>
        </div>
        <div class="header-tabbed">
            <div class="container">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" href="profile">profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="catalog">catalog</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <h3><?php echo $title; ?><h3>
                        <p><?php echo $description; ?></p>
                        <form method="POST">
                            <div class="form-group">
                                <label for="x">X <span class="text-danger">*</span></label>
                                <input type="text" name="x" id="x" value="<?php echo $params_x; ?>" class="form-control" required autofocus />
                            </div>

                            <div class="form-group">
                                <label for="y">Y <span class="text-danger">*</span></label>
                                <input type="text" name="y" id="y" value="<?php echo $params_y; ?>" class="form-control" required autofocus />
                            </div>

                            <div class="form-group">
                                <label for="z">Z <span class="text-danger">*</span></label>
                                <input type="text" name="z" id="z" value="<?php echo $params_z; ?>" class="form-control" required autofocus />
                            </div>

                            <div class="form-group">
                                <label for="quantity">Quantity <span class="text-danger">*</span></label>
                                <input type="text" name="quantity" id="quantity" value="<?php echo $params_quantity; ?>" class="form-control" required autofocus />
                            </div>

                            <div class="alert alert-danger"><?php echo $params_err; ?></div>

                            <button class="btn btn-primary pull-right" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
    </body>
</html>
