<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Profile - AK CONSULT</title>
        <link rel="stylesheet" href="static/main.css" />
    </head>
    <body>
        <nav class="container navbar navbar-light navbar-expand-sm">
            <div class="login">
                <span class="navbar-text">Logged in as <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b> — <a href="logout">Log out</a></span>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <p>You can manage your AK CONSULT account here.</p>
                    </section>
                </div>
            </div>
        </div>
        <div class="header-tabbed">
            <div class="container">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="profile">profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="catalog">catalog</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h3>User Profile</h3>
                    <form method="POST">
                        <div class="form-group">
                            <label for="username">Username <span class="text-muted">(you can't edit this)</span> </label>
                            <input type="text" class="form-control" id="username" value="<?php echo htmlspecialchars($_SESSION["username"]); ?>" readonly />
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
