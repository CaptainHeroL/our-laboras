<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true || $_SESSION["username"] != "admin") {
    header("location: ../login");
    exit;
}

require_once "config.php";
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="theme-color" content="#ffffff"/>
        <title>Catalog - AK CONSULT</title>
        <link rel="stylesheet" href="../static/main.css"/>
    </head>
    <body>
        <nav class="container navbar navbar-light navbar-expand-sm">
            <div class="login">
                <span class="navbar-text">
                    Logged in as <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b> — <a href="../logout">Log out</a>
                </span>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <p>A list of past and active orders.</p>
                    </section>
                </div>
            </div>
        </div>
        <div class="header-tabbed">
            <div class="container">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" href="profile">profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="catalog">catalog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="orders">orders</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="event-list">
                        <?php
                        $result = mysqli_query($link, 'SELECT * FROM orders');
                        while ($row = mysqli_fetch_array($result)) {
                            echo '<div class="event"><h4><a href="order?id=' . $row['id'] . '">' . $row['username'] . "</a><p>" . ($row['pending']) ? 'pending' : 'completed' . "</p></h4></div>";
                        }
                        mysqli_close($link);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
